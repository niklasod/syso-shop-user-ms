package se.syso.shop.user.web;

import org.springframework.stereotype.Component;
import se.syso.shop.web.EventRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Component
@Path("/event/v1")
public class Endpoint {

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response handleEvent(EventRequest eventrequest) {
        return Response.ok("\"status\":\"OK\"").build();
    }
}
