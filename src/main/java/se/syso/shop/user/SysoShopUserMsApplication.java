package se.syso.shop.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SysoShopUserMsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SysoShopUserMsApplication.class, args);
    }
}
