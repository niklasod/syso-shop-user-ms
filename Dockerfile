#FROM openjdk:8-jdk-alpine
FROM maxleiko/armhf-alpine-java
#VOLUME /tmp
COPY ./target/syso-shop-user-ms-0.0.1-SNAPSHOT.jar user.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/.urandom","-jar","user.jar"]
